//
//  PlaceholderTextfield.m
//  CustomTextField
//
//  Created by Rohit Kumar Modi on 02/12/15.
//  Copyright © 2015 Rohit Kumar Modi. All rights reserved.
//

#import "PlaceholderTextfield.h"

@implementation PlaceholderTextfield

#pragma mark - Initialization
- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    _horizontalLeftPadding = 3;
    _horizontalRightPadding = 0;
    NSLog(@"%@",self.font.fontName);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:nil];
    [self setupFloatLabel];
}

- (void)setupFloatLabel
{
    
    // Default colors
    _passiveColor = [UIColor lightGrayColor];
    _activeColor = [UIColor blueColor];
    _lastActiveColor = _activeColor;
   
    // floatLabel
    _floatLabel = [UILabel new];
    _floatLabel.text = self.placeholder;
//    self.placeholder = _floatLabel.text;
    _floatLabel.textColor = _passiveColor;
//    _floatLabel.font =self.font;
    //default font
//    self.backgroundColor = [UIColor lightGrayColor];
    _floatLabel.font = [UIFont fontWithName:@"Helvetica" size:10.0];
    _myFont = [UIFont fontWithName:@"Helvetica" size:10.0];
    _floatLabel.alpha = 0.0f;
    [self changeFrame];
    [self addSubview:_floatLabel];
   
    [_floatLabel sizeToFit];
}

-(void)changeFrame{
    _passiveRect = CGRectMake(_horizontalLeftPadding,
                              8,
                              CGRectGetWidth([_floatLabel frame]),
                              CGRectGetHeight([_floatLabel frame]));
    _activeRect = CGRectMake(_horizontalLeftPadding,
                             0,
                             CGRectGetWidth([_floatLabel frame]),
                             CGRectGetHeight([_floatLabel frame]));
    
    _floatLabel.frame = _passiveRect;
}

-(void)customFontNameWithSize:(UIFont*)myFont{
    _myFont = myFont;
//    [_floatLabel sizeToFit];
}

-(void)customActiceColor:(UIColor*)myColor{
    _activeColor = myColor;
    _lastActiveColor = _activeColor;
}

-(void)customPassiveColor:(UIColor*)myColor{
    _passiveColor = myColor;
    _floatLabel.textColor = _passiveColor;
}

-(void)customFrame:(CGRect)activeFrame passiveFrame:(CGRect)passiveFrame{
    _activeRect = activeFrame;
    _passiveRect = passiveFrame;
}

- (void)textDidChange:(NSNotification *)notification
{
    
    NSLog(@"%@",[self text]);
    if (notification.object == self) {
        if ([self.text length]) {
            if (![_floatLabel alpha]) {
                [UIView animateWithDuration:0.25
                                 animations:^(void) {
                                     _floatLabel.textColor = _activeColor;
                                     _belowLineLabel.backgroundColor = _lineActiveColor;
                                     self.placeholder = @"";
                                     _floatLabel.alpha = 1.0;
                                     _floatLabel.font =[UIFont fontWithName:_myFont.fontName size:10.0];
                                     _floatLabel.frame = _activeRect;
                                      [_floatLabel sizeToFit];
                                 }];

            }
        } else {
            if ([_floatLabel alpha]) {
                [UIView animateWithDuration:0.05
                                 animations:^(void) {
                                     _floatLabel.textColor = _passiveColor;
                                     _floatLabel.font = _myFont;

                                     _floatLabel.frame = _passiveRect;
                                     [_floatLabel sizeToFit];
                                
                                 }
                                 completion:^(BOOL completed) {
                                     _floatLabel.alpha = 0.0f;
                                     
                                     self.placeholder = _floatLabel.text;
                                 }];
            }
        }
    }
}

#pragma mark - Breakdown
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:nil];
}

#pragma mark - UIResponder (Override)
-(BOOL)becomeFirstResponder
{
    [super becomeFirstResponder];
    _floatLabel.textColor = _activeColor;
    _belowLineLabel.backgroundColor = _lineActiveColor;

//    _belowLineLabel.frame = CGRectMake(0,
//                                       self.frame.size.height - 2,
//                                       self.frame.size.width,
//                                       2);
    return YES;
}

- (BOOL)resignFirstResponder
{
    _floatLabel.textColor = _passiveColor;
    _belowLineLabel.backgroundColor = _linePassiveColor;
    _activeColor = _lastActiveColor;
    _lineActiveColor = _lastLineActiveColor;
    
//    _belowLineLabel.frame = CGRectMake(0,
//                                       self.frame.size.height - 1,
//                                       self.frame.size.width,
//                                       1);
    [super resignFirstResponder];
    
    return YES;
}

-(void)paddingFromLeft:(int)leftPadding fromRight:(int)rightPadding{
    UIView *fromLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftPadding, 0)];
    self.leftView = fromLeft;
    self.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *fromRight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rightPadding, 0)];
    self.rightView = fromRight;
    self.rightViewMode = UITextFieldViewModeAlways;
    
    
    _horizontalLeftPadding = leftPadding + 1;
    
    [self changeFrame];
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    //    if (action == @selector(paste:)) {
    //        return NO;
    //    }

//    if (action == @selector(paste:)) { // Toggle Pasting
//        return ([_pastingEnabled boolValue]) ? YES : NO;
//    } else if (action == @selector(copy:)) { // Toggle Copying
//        return ([_copyingEnabled boolValue]) ? YES : NO;
//    } else if (action == @selector(cut:)) { // Toggle Cutting
//        return ([_cuttingEnabled boolValue]) ? YES : NO;
//    } else if (action == @selector(select:)) { // Toggle Select
//        return ([_selectEnabled boolValue]) ? YES : NO;
//    } else if (action == @selector(selectAll:)) { // Toggle Select All
//        return ([_selectAllEnabled boolValue]) ? YES : NO;
//    }
    
    return [super canPerformAction:action withSender:sender];
}

- (BOOL)isEmpty {
    return ([self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]].length == 0) ? YES : NO;
}

- (BOOL)isValidEmail {
    
    NSString *emailRegEx = @"(?:[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[A-Za-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[A-Za-z0-9](?:[A-Za-"
    @"z0-9-]*[A-Za-z0-9])?\\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[A-Za-z0-9-]*[A-Za-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    return [emailTest evaluateWithObject:self.text];
    
}

-(void)belowLineWithActiveColor:(UIColor*)activeColor passiveColor:(UIColor*)passiveColor isTextfieldHeightFixed:(BOOL)fixed{
    
    _lineActiveColor = activeColor;
    _lastLineActiveColor = _lineActiveColor;
    _linePassiveColor = passiveColor;
    // _belowLineLabel
    _belowLineLabel = [UILabel new];
    
    if (fixed) {
        
        //    ********************** If set textfield in fix height **************************
        _belowLineLabel.frame = CGRectMake(0, self.frame.size.height - 2, ((self.frame.size.width / 320) * [[UIScreen mainScreen] bounds].size.width) + ((320 - self.frame.size.width )/2), 2);
        //    ********************** end **************************
    }
    else{
        //    ********************** If set textfield in aspect fit **************************
        if ([[UIScreen mainScreen] bounds].size.height == 480) {
            _belowLineLabel.frame = CGRectMake(0, self.frame.size.height - 2, ((self.frame.size.width / 320) * [[UIScreen mainScreen] bounds].size.width) + ((320 - self.frame.size.width )/2), 2);
        }
        else{
            _belowLineLabel.frame = CGRectMake(0, ((self.frame.size.height / 480) * [[UIScreen mainScreen] bounds].size.height) - ((480 / self.frame.size.height )/2) - 4, ((self.frame.size.width / 320) * [[UIScreen mainScreen] bounds].size.width) + ((320 - self.frame.size.width )/2), 2);
        }
        //    ********************** end **************************
    }
    
    _belowLineLabel.backgroundColor = _linePassiveColor;
    [self addSubview:_belowLineLabel];
    
}

-(void)setErrorColor:(UIColor*)myColor{

    _errorColor = myColor;
    _activeColor = _errorColor;
    _lineActiveColor = _errorColor;
    _floatLabel.textColor = _activeColor;
    _belowLineLabel.backgroundColor = _activeColor;
    
//    _belowLineLabel.frame = CGRectMake(0,
//                                       self.frame.size.height - 2,
//                                       self.frame.size.width,
//                                       2);
}
@end
