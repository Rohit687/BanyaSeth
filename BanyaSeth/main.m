//
//  main.m
//  BanyaSeth
//
//  Created by Rohit Kumar Modi on 28/11/15.
//  Copyright © 2015 Rohit Kumar Modi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
