//
//  ViewController.m
//  BanyaSeth
//
//  Created by Rohit Kumar Modi on 28/11/15.
//  Copyright © 2015 Rohit Kumar Modi. All rights reserved.
//

#import "LoginViewController.h"
#import "UIImage+deviceSpecificMedia.h"

@interface LoginViewController ()<UITextFieldDelegate,BSKeyboardControlsDelegate>
{
    NSArray *textFields;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *bgView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet PlaceholderTextfield *userEmail;
@property (weak, nonatomic) IBOutlet PlaceholderTextfield *userPassword;

@property (weak, nonatomic) IBOutlet UILabel *errorEmail;
@property (weak, nonatomic) IBOutlet UILabel *errorPassord;

@property (weak, nonatomic) IBOutlet UIView *mainForgotView;
@property (weak, nonatomic) IBOutlet UIView *forgotView;
@property (weak, nonatomic) IBOutlet PlaceholderTextfield *forgotEmail;
@property (weak, nonatomic) IBOutlet UILabel *forgotErrorEmail;

@property (nonatomic, strong) BSKeyboardControls *keyboardControls;
@end

@implementation LoginViewController
@synthesize scrollView, bgView, mainView, userEmail, userPassword, errorEmail, errorPassord;
@synthesize mainForgotView, forgotView, forgotEmail, forgotErrorEmail;

#pragma mark - View life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    errorEmail.hidden = YES;
    errorPassord.hidden = YES;
    forgotErrorEmail.hidden = YES;
    
    [self viewFrame];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)setupCustomTextfield:(PlaceholderTextfield*)textfield{
    
    [textfield setup];
    [textfield customActiceColor:[UIColor colorWithRed:72.0/255.0 green:203.0/255.0 blue:216.0/255.0 alpha:1.0]];
    [textfield customPassiveColor:[UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:1.0]];
    [textfield paddingFromLeft:5 fromRight:0];
    [textfield belowLineWithActiveColor:[UIColor colorWithRed:86.0/255.0 green:240.0/255.0 blue:255.0/255.0 alpha:1.0] passiveColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0] isTextfieldHeightFixed:true];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    textFields = @[userEmail,userPassword];
    [self setKeyboardControls:[[BSKeyboardControls alloc] initWithFields:textFields]];
    [self.keyboardControls setDelegate:self];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewFrame{
    
    mainForgotView.hidden = YES;
    forgotView.translatesAutoresizingMaskIntoConstraints = YES;
    forgotView.frame = CGRectMake((self.view.frame.size.width/2) - (300/2) , -264, 300, 264);
    
    [self setupCustomTextfield:userEmail];
    [self setupCustomTextfield:userPassword];
    [self setupCustomTextfield:forgotEmail];
}
#pragma mark - end

#pragma mark - Keyboard Controls Delegate
- (void)keyboardControls:(BSKeyboardControls *)keyboardControls selectedField:(UIView *)field inDirection:(BSKeyboardControlsDirection)direction
{
    UIView *view;
    
    if ([[UIDevice currentDevice].systemVersion floatValue]< 7.0) {
        view = field.superview.superview;
    } else {
        view = field.superview.superview.superview;
    }
}

- (void)keyboardControlsDonePressed:(BSKeyboardControls *)keyboardControls
{
    [keyboardControls.activeField resignFirstResponder];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - end

#pragma mark - Textfield Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [self.keyboardControls setActiveField:textField];
    if (textField != forgotEmail) {
        [scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y+( textField.frame.size.height)) animated:YES];
    }
    else{
        if ([[UIScreen mainScreen] bounds].size.height == 480) {
            mainForgotView.translatesAutoresizingMaskIntoConstraints = YES;
            mainForgotView.frame = CGRectMake(0, -40, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    if (textField == userEmail) {
        errorEmail.hidden = YES;
    }
    else if (textField == userPassword) {
        errorPassord.hidden = YES;
    }
    else if (textField == forgotEmail) {
        forgotErrorEmail.hidden = YES;
    }
    
    if (textField == forgotEmail) {
        if ([[UIScreen mainScreen] bounds].size.height == 480) {
             mainForgotView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            mainForgotView.translatesAutoresizingMaskIntoConstraints = NO;
           
        }

    }
   
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    
    return YES;
    
}

#pragma mark - end

#pragma mark - UIView action
- (IBAction)forgot:(UIButton *)sender {
    [self.view endEditing:YES];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    forgotEmail.text = @"";
    forgotErrorEmail.hidden = YES;
    [self showViewAnimation];
}

- (IBAction)login:(UIButton *)sender {
    [self.view endEditing:YES];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    if ([self performValidationsForLogin]) {
        [myDelegate showIndicator];
        [self performSelector:@selector(loginUser) withObject:nil afterDelay:5.0];
    }
}

- (IBAction)registerView:(UIButton *)sender {
}

- (IBAction)visitAsGuest:(UIButton *)sender {
}

- (IBAction)facebook:(UIButton *)sender {
}

- (IBAction)cancel:(UIButton *)sender {
    
    [self.view endEditing:YES];
    [self hideViewAnimation];
}

- (IBAction)forgotSubmit:(UIButton *)sender {
    
    [self.view endEditing:YES];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    if ([self performValidationsForForgot]) {
        [myDelegate showIndicator];
        [self performSelector:@selector(forgotUser) withObject:nil afterDelay:5.0];
    }
    
}
#pragma mark - end

#pragma mark - Textfield valdation
- (BOOL)performValidationsForLogin
{
    
    if ([userEmail isEmpty] || (userEmail.text.length==0) || ([userEmail.text isEqualToString:@""]))
    {
        errorEmail.text = @"Oops! Email is required.";
        [userEmail setErrorColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
        errorEmail.hidden = NO;
        return NO;
    }
    else  if ([userPassword isEmpty] || (userPassword.text.length==0) || ([userPassword.text isEqualToString:@""]))
    {
        errorPassord.text = @"Oops! Password is required.";
        [userPassword setErrorColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
        errorPassord.hidden = NO;

        return NO;
    }
    
    else
    {
        if ([userEmail isValidEmail])
        {
            if (userPassword.text.length<6 )
            {
                
                errorPassord.text = @"Your password must be atleast 6 characters long.";
                [userPassword setErrorColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
                errorPassord.hidden = NO;
                
                return NO;
                
            }
            else
            {
                return YES;
            }
        }
        else
        {
            
            errorEmail.text = @"Oh Oh! Please enter a valid email address.";
            [userEmail setErrorColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
            errorEmail.hidden = NO;
            return NO;
        }
        
    }
    
}

- (BOOL)performValidationsForForgot
{
    
    if ([forgotEmail isEmpty] || (forgotEmail.text.length==0) || ([forgotEmail.text isEqualToString:@""]))
    {
        forgotErrorEmail.text = @"Oops! Email is required.";
        [forgotEmail setErrorColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
        forgotErrorEmail.hidden = NO;
        return NO;
    }
    else
    {
        if ([forgotEmail isValidEmail])
        {
            return YES;
            
        }
        else
        {
            
            forgotErrorEmail.text = @"Oh Oh! Please enter a valid email address.";
            [forgotEmail setErrorColor:[UIColor colorWithRed:255.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0]];
            forgotErrorEmail.hidden = NO;
            return NO;
        }
        
    }
    
}
#pragma mark - end

#pragma mark - AlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1)
    {
        [self hideViewAnimation];
    }
    
}
#pragma mark - end

#pragma mark - Web service
-(void)forgotUser{
    [myDelegate stopIndicator];
    if ([forgotEmail.text isEqualToString:@"rohitroyal28@gmail.com"]) {
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Password is sent in email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        alert.tag = 1;
        [alert show];
        
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email and password is an incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}


-(void)loginUser{
    [myDelegate stopIndicator];
    if ([userEmail.text isEqualToString:@"rohitroyal28@gmail.com"]) {
        
    }
    else{
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Email and password is an incorrect." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
#pragma mark - end

#pragma mark - Show and hide animation
-(void)showViewAnimation{

    mainForgotView.hidden = NO;
    forgotView.alpha = 1.0;
    forgotView.transform = CGAffineTransformIdentity;
    CGRect startFrame = CGRectMake((self.view.frame.size.width/2) - (300/2) , ((self.view.frame.size.height/2) - (264/2)), 300, -264);
    //    startFrame.origin.y = -CGRectGetHeight(finalContainerFrame);
    forgotView.frame = startFrame;
    
    [UIView animateWithDuration:1.0
                          delay:0.0
         usingSpringWithDamping:0.5
          initialSpringVelocity:10.0
                        options:0
                     animations:^{
                         forgotView.frame = CGRectMake((self.view.frame.size.width/2) - (300/2) , ((self.view.frame.size.height/2) - (264/2)), 300, 264);
                     }
                     completion:nil];
    
}

-(void)hideViewAnimation{

    [UIView animateWithDuration:0.13
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^(void){
                         forgotView.frame = CGRectMake((self.view.frame.size.width/2) - (300/2) , ((self.view.frame.size.height/2) - (264/2)) - 40, 300, 264);
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.26
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseIn
                                          animations:^(void){
                                              forgotView.frame = CGRectMake((self.view.frame.size.width/2) - (300/2) , self.view.frame.size.height + 40, 300, 264);
                                          }
                                          completion:^(BOOL finished){
                                              mainForgotView.hidden = YES;
                                          }];
                     }];

}
#pragma mark - end

@end
