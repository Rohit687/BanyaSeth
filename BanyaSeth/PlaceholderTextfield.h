//
//  PlaceholderTextfield.h
//  CustomTextField
//
//  Created by Rohit Kumar Modi on 02/12/15.
//  Copyright © 2015 Rohit Kumar Modi. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef UIFloatLabelAnimationType
typedef NS_ENUM(NSUInteger, UIFloatLabelAnimationType)
{
    UIFloatLabelAnimationTypeShow = 0,
    UIFloatLabelAnimationTypeHide
};
#define UIFloatLabelAnimationType UIFloatLabelAnimationType
#endif

@interface PlaceholderTextfield : UITextField

@property (nonatomic, assign) CGFloat horizontalLeftPadding;

@property (nonatomic, assign) CGFloat horizontalRightPadding;

@property (nonatomic, strong) UILabel *floatLabel;

@property (nonatomic, strong) UILabel *belowLineLabel;

/**
 * The inactive color for the floatLabel.
 *
 * Defaults to @c lightGrayColor.
 */
@property (nonatomic, strong) UIColor *passiveColor;

/**
 The inactive color for the floatLabel.
 *
 * Defaults to @c blueColor.
 */
@property (nonatomic, strong) UIColor *activeColor;

@property (nonatomic, strong) UIColor *lastActiveColor;

@property (nonatomic, strong) UIColor *errorColor;

@property (nonatomic, strong) UIColor *linePassiveColor;

@property (nonatomic, strong) UIColor *lineActiveColor;

@property (nonatomic, strong) UIColor *lastLineActiveColor;

@property (nonatomic, strong) UIFont *myFont;

@property (nonatomic, assign) CGRect activeRect;

@property (nonatomic, assign) CGRect passiveRect;

- (void)setup;
-(void)customFontNameWithSize:(UIFont*)myFont;
-(void)customActiceColor:(UIColor*)myColor;
-(void)customPassiveColor:(UIColor*)myColor;
-(void)setErrorColor:(UIColor*)myColor;
-(void)customFrame:(CGRect)activeFrame passiveFrame:(CGRect)passiveFrame;
-(void)paddingFromLeft:(int)leftPadding fromRight:(int)rightPadding;

-(void)belowLineWithActiveColor:(UIColor*)activeColor passiveColor:(UIColor*)passiveColor isTextfieldHeightFixed:(BOOL)fixed;

- (BOOL)isEmpty;
- (BOOL)isValidEmail;
@end
