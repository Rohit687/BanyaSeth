//
//  AppDelegate.h
//  BanyaSeth
//
//  Created by Rohit Kumar Modi on 28/11/15.
//  Copyright © 2015 Rohit Kumar Modi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)showIndicator;
- (void)stopIndicator;

@end

